definition module iTasks.UI.Layout.Default
/**
* This module defines the functions that drive iTask's automagic default UI layouting
*/

import iTasks.UI.Layout

defaultSessionLayout    :: Layout //Added when a task instance is 'published' (can be easily removed or replaced by publishing a task explicitly)

finalizeUI 				:: Layout
finalizeInteract 		:: Layout
finalizeStep 			:: Layout
finalizeParallel 		:: Layout
