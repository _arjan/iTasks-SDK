definition module Tests.Interactive.Layout
import iTasks, iTasks.Internal.Test.Definition

testLayoutI :: TestSuite

//Additional publishable tasks that you can use to interactively test the core layout operations
layoutTestTasks :: [PublishedTask]
