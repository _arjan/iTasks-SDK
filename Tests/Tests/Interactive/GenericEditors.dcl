definition module Tests.Interactive.GenericEditors
/**
* Tests for the generic editors of standard datatypes
*/
import iTasks, iTasks.Internal.Test.Definition

testGenericEditors :: TestSuite
