definition module Tests.Interactive.BuiltinContainers
/**
* Tests for the builtin containers in the client library
*/
import iTasks, iTasks.Internal.Test.Definition

testBuiltinContainers :: TestSuite
